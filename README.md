# Vagrant boxes for Prologin (using packer)

Forked from [elasticdog/packer-arch](https://github.com/elasticdog/packer-arch.git).

## Overview

This is a [packer](https://www.packer.io/) build environment for Archlinux.
It also includes the *pacaur* AUR helper.

## Usage

### VirtualBox Provider

Dependencies: Virtualbox

    $ make virtualbox
    $ vagrant box add arch output/packer_arch_virtualbox.box

### VMware Provider

Dependencies: [VMware Fusion](https://www.vmware.com/products/fusion/) (or
[VMware Workstation](https://www.vmware.com/products/workstation/))

    $ make vmware
    $ vagrant box add arch output/packer_arch_vmware.box

### Parallels Provider

Dependencies: [Parallels](http://www.parallels.com/), [Parallels SDK](http://www.parallels.com/eu/products/desktop/download/)

    $ make parallels
    $ vagrant box add arch output/packer_arch_parallels.box

### libvirt Provider

Dependencies: [vagrant-libvirt](https://github.com/vagrant-libvirt/vagrant-libvirt)

    $ make libvirt
    $ vagrant box add arch output/packer_arch_libvirt.box

NOTE: libvirt support is limited to QEMU/KVM only.

## License

This is provided under the terms of the
[ISC License](https://en.wikipedia.org/wiki/ISC_license).

