arch-template.json: arch-template.yml
	ruby -ryaml -rjson -e 'puts JSON.generate(YAML.load(ARGF))' <$< >$@

virtualbox: arch-template.json
	packer-io build -only=virtualbox-iso -var headless=true -var disk_size=$(DISK_SIZE) $<

vmware: arch-template.json
	packer-io build -only=vmware-iso -var headless=true -var disk_size=$(DISK_SIZE) $<

parallels: arch-template.json
	packer-io build -only=parallels-iso -var headless=true -var disk_size=$(DISK_SIZE) $<

libvirt: arch-template.json
	packer-io build -only=libvirt -var headless=true -var disk_size=$(DISK_SIZE) $<

clean:
	$(RM) arch-template.json

.PHONY: virtualbox vmware parallels libvirt clean

